---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}

type: "docs"
no_list: true
---
