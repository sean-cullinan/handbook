---
title: "Handbook"
linkTitle: "Handbook"

type: "docs"
no_list: true

weight: 20
menu:
  main:
    weight: 20
---

# Introduction

The intelliHR team handbook is the central repository for how the company is
run. We welcome feedback from anyone. If you have any suggestions,
improvements or notice something is wrong, please
[open a merge request]({{< param github_repo >}}/merge_requests)
and help us improve it.

- [Engineering]({{< relref "/handbook/engineering" >}})
- [Human Resources]({{< relref "/handbook/hr" >}})
  - [Policies]({{< relref "/handbook/hr/policies" >}})
    - [COVID Safe Plan]({{< relref "/handbook/hr/policies/covid-safe-plan" >}})
- Customer Success
  - Product feedback
- Marketing
- Sales
