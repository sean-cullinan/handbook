---
title: "Engineering"
date: 2020-08-04T23:56:24Z

type: "docs"
no_list: true
---

- Bug reporting and fixing
- Development setup
  - Linux
  - macOS
  - Windows
- Encrypting SSM params
- [Migration to GitLab]({{< relref "/handbook/engineering/migration-to-gitlab" >}})
- Releasing
  - Hotfixing
  - Release process
- [Roadmap]({{< relref "/handbook/engineering/roadmap" >}})
- Services
  - Command Centre
  - Graphite
  - Lapis
  - PAPI
  - SPA
- Task break down
  - [Task weighting]({{< relref "/handbook/engineering/weights/" >}})
- Recruitment
  - End of probation
  - 6 month reviews
- Position titles
  - Software Engineers
    - Graduate Software Engineer
    - Software Engineer I
    - Software Engineer II
    - Senior Software Engineer
  - Engineering Leaders
    - Team Leader
    - Engineering Manager
- [Workflow]({{< relref "/handbook/engineering/workflow" >}})
  - Planning
  - [Team Retrospectives]({{< relref "/handbook/engineering/team-retrospectives" >}})
