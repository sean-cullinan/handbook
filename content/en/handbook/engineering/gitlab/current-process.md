---
title: "Current Process"
date: 2020-08-04T23:59:40Z

type: "docs"
no_list: true
draft: true
---

Starting from the kick-off of a new sprint the domain team will first check
Airtable for any new feedback items from customers. These new items will be
discussed within the team and if it is decided that it fits in with the product
strategy then the team will create a new card in Favro for the deliverable. The
card in Airtable will have its column updated and the next card will be
reviewed. If a card has been decided that it won't be worked on then it is moved
into a `Won't do` column along with a comment about why the feedback item
doesn't align with the companies strategy. Larger feedback items may be marked
as a `Roadmap Deliverable` which means it is something that is already planned
and should be considered in the future when that work is going to be started.

These card in Airtable can then be looked at by customer success to see what has
been moved and can report anything relevant back to the original customers.

The delivery milestones within Favro are organised based on their position
within the company roadmap. Any completed milestones are archived and the
upcoming milestones are discussed to ensure that there is enough context and
defined scope for the work to begin. After the team planning session, there may
be 1 or more "breakdown" sessions where the team can break these cards down into
smaller pieces of work that can be picked up by team members.

As these cards are picked up they move along the teams kanban board. Once a card
has been merged then an 'estimate' is added to the card. When a card has beed
released into production the card is archived.

During a sprint a number of bug cards may be added by customer success which end
up on a teams board. These are prioritised by the teams based on the importance
of the bug report and may take priority over existing development work. These
bugs originally come from Zendesk, which the engineering teams do not have
access to, instead, the engineers will converse with customer success for
information when needed.
