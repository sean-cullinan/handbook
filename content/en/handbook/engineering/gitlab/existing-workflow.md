---
title: "Existing Workflow"
date: 2020-08-04T23:59:40Z

type: "docs"
no_list: true
draft: true
---

Due to the mixed nature of the products that we use, our existing workflow is
disjointed and requires accessing different products for the different parts of
the flow.

## Product feedback

Product feedback is captured via a public Airtable form. Submitted feedback is
captured and saved in to an Airtable sheet which is groomed during the domain
teams sprint planning sessions.

## Development work

During sprint planning sessions teams comb the feedback added to Airtable within
the last 2 weeks and after discussion create new cards within Favro that they
believe will be of value as delivery milestones (epics).

As the delivery milestones are prioritised they will be broken down into new
cards. These cards are created within a teams board for individual chunks of
work that can be actioned. Favro is customisable and we have added a number of
extra fields to the cards that we use to track various bits of information,
these include things such as:

- Airtable (link back to an Airtable record)
- Design (are designed needed and what stage are they at)
- Feedback request  (link back to original feedback item)
- Figma (link to a Figma design)
- Miro (link to a relevant Miro board)
- PR (repeated multiple times to link to code changes)
- Slack (link to a Slack message)
- Status (progress of the delivery milestone)

## Bug reports

Bug reports are submitted to Zendesk where a member of the Customer Success team
will triage them and report them to the engineering team. When a report has been
confirmed as a valid bug the Customer Success team member will then create a new
card in Favro which will then be assigned to the domain team that will handle
the fix for the issue.
