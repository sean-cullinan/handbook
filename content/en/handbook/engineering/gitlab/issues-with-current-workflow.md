---
title: "Issues with current workflow"
date: 2020-08-04T23:59:40Z

type: "docs"
no_list: true
draft: true
---

- Airtable updates missed
- Linkage between Favro and Airtable
- Linkage between Favro Delivery Milestone and Breakdown cards
- Linkage between Favro and Zendesk reports
- Grouping of work for R&D

## Disconnection between platforms

Using product feedback items as an example, we have the reports coming in via an
online form into Airtable. As these are actioned new cards are created in Favro
for the engineering teams. The original Airtable record may then be marked as
actioned and moved on its board, but there is no association between the
Airtable record and the Favro card. This makes it hard for Customer Success to
report back to customers about the status of feedback items. Due to the nature
of Favro customisations it is also hard to have a relationship from a single
development milestone card back to multiple Airtable feedback items without
needing to create multiple custom fields with the same name.

There is a similar disconnection between bug reports in Zendesk and the cards in
Favro.

## Tracking of breakdown cards to a delivery milestone

Favro has the concept of breaking a card down into a board of its own. In
principle this would allow a delivery milestone to become its own Kanban board
which would track all the work done towards that milestone, however, in practice
this results in a very awkward process where teams would need to manage multiple
boards at a time.

Labels in Favro can be used to try and track related cards. For example there
may be a delivery milestone to deliver the editing of a completed task. A new
label could be create called `Edit Completed Task` which would then need to be
applied to all card for that milestone. Again, in practice, this is not an ideal
workflow and for reporting does no produce reliable results.

## Linking of cards between themselves

Often when working on a new feature there will be work that needs to be
completed before further work can be started. Without an explicit linkage
between cards in Favro there is no standardised way to show which cards are
needed first and the extent of tracking this is adding the label `Blocked` to
cards.

## End of year reporting

At the end of the financial year we need to report on the work done by then
engineering teams. Because of the issues listed above this is currently a time
consuming process that requires a lot of manual checking to ensure that there
aren't any mistakes within the reports.
