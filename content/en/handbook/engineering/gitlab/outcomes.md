---
title: "Outcomes"
date: 2020-08-04T23:59:40Z

type: "docs"
no_list: true
draft: true
---

The number one outcome of a migration to GitLab would be to consolidate some of
these products into a single location. This would allow a more streamlined
process for both the engineering teams, team managers and customer success
teams.

## Favro cards to GitLab issues

One of the first steps in this migration would be to move the process currently
managed by Favro into GitLab.

GitLab issues can reproduce Favro cards in a very similar manner. Individual
issues/cards would work almost identically. GitLab issues have the ability to
add labels for organisation and assign weights to track work required.
Individual boards for each of the teams can be created that show only the cards
for that domain and can be customised to represent the workflow that the teams
follow for their Kanban process.

There are a number of advantages of moving to GitLab issues. GitLab has the
concept of *Epics*. These can be used as a replacement for the existing Delivery
Milestones. Epics natively support a relationship to issues that can be used to
track the work spent to deliver on that project. Epics will automatically track
the dates of cards that are related to it, allowing us to see how long a project
has taken.

In GitLab there are
[Milestones](https://docs.gitlab.com/ee/user/project/milestones/#milestones-as-agile-sprints)
that can be used as Sprint time boxes. Sprints in Favro are tracked using a
label and don't offer a way to see the dates that a particular Sprint was
active.

Individual issues within GitLab can have many relationships to other cards and
can be set as either being `related` or `blocking`/`blocked by` allowing a clear
reference between an issue and why it is currently un-workable.

## Product Feedback as GitLab issues

Moving Product Feedback out of Airtable and into GitLab issues will allow a much
tighter bond between the engineering teams and customer success teams. As
product feedback is groomed within GitLab and new Epics are started a direct
link between them can be established. This will greatly help customer success
report the status of feedback items back to the customers that had reported it.

## Bug reports as GitLab issues

Similar to the product feedback reports, moving bug reports into GitLab issues
would again give a much more streamlined process in handling and management of
the reports.

## Direct relationships between code and issues

As a secondary outcome, using GitHub for code and Favro for card management
results in a weak link between merge requests and the original card. Being able
to reference issues directly within commit messages and having GitLab
understanding the message and taking action on it could help engineers workflow.

## End of year reporting

A major outcome expected of a possible migration would be to vastly reduce the
amount of time needed to produce end of year reports due to a reduction in the
manual work required to check Favro cards.
