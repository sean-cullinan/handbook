---
title: "Products in use"
date: 2020-08-04T23:59:40Z

type: "docs"
no_list: true
draft: true
---

- [Airtable](https://airtable.com) - product feedback from customers
- [Favro](https://www.favro.com) - engineering tasks and high level deliverables
- [GitHub](https://github.com) - source code repositories
- [Zendesk](https://www.zendesk.com) - support requests from customers

### Airtable

Airtable is used to collect feedback from customers, both internal and external.
From here the engineering domains teams will go through collected items and add
them to the backlog based on priority, or reject them if they are not deemed to
align with the product strategy.

### Favro

Favro is used by engineering teams to prioritise their upcoming delivery
milestones as well as breakdown this work into smaller, task based cards. Favro
is then used to track the progress of the individual cards in the individual
team kanban boards.

Favro is also used by customer success to inform the engineering team of bugs
reported by the customers. These are added to a generic bug board where the
cards are then moved into the teams board who will take on the work to fix the
issue.

### GitHub

GitHub is used as the source code repository for all of the intelliHR
application code. It is used for handling the reviewing of code as pull requests
are opened.

### Zendesk

Zendesk is where customer reported bugs are stored and handled by the customer
success team. Once a bug has been confirmed customer success will create a card
in Favro for the engineers to work on.
