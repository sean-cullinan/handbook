---
title: "Migration to GitLab"
date: 2020-08-04T23:59:40Z

type: "docs"
no_list: false
---

- [Background](#background)
- [Current products in use](#current-products-in-use)
- [Existing workflow](#existing-workflow)
- [Problems we wish to solve](#problems-we-wish-to-solve)
- [Changes](#changes)


## Background

Across intelliHR we make use of a number of products to handle our workflow processes. In order to improve the existing flow we want to simplify and enhance them by consolidating a number of these into GitLab.


## Current products in use

- [Airtable](https://airtable.com) - product feedback from customers
- [Favro](https://www.favro.com) - engineering tasks and high level deliverables
- [GitHub](https://github.com) - source code repositories
- [Zendesk](https://www.zendesk.com) - support requests from customers


## Existing workflow

- Product feedback is captured in Airtable
- Bug reports are handled in Zendesk
- Delivery milestones are in Favro
- Breakdown cards are in Favro (and disconnected to delivery milestones)


## Problems we wish to solve

- Updates in one product can be easily missed by interested parties
- There is no connection between a piece of work back to its parent epic
- Tracking related pieces of work with each other
- Reduce the amount of manual work needed to produce EOFY reports, and increase the accuracy
- Improve visibility of work being done and road map overview


## Changes

- Favro delivery milestones to become [GitLab epics](https://docs.gitlab.com/ee/user/group/epics/index.html)
- Favro work cards to become [GitLab issues](https://docs.gitlab.com/ee/user/project/issues/)
- Sprint tracking via [GitLab milestones](https://docs.gitlab.com/ee/user/project/milestones/#milestones-as-agile-sprints)
- Product feedback to be added directly into GitLab via [Service Desk](https://docs.gitlab.com/ee/user/project/service_desk.html)
