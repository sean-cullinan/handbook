---
title: "Epics"
description: "How will Delivery Milestones work as Epics within GitLab"
date: 2020-08-19T12:02:17Z

type: "docs"
no_list: true
---

[Epics](https://docs.gitlab.com/ee/user/group/epics/index.html) will be the direct replacement for what we currently call Delivery Milestones. These will be used to track larger projects that teams are working on. Epics within GitLab provide a number of improvements over our existing usage of Favro cards and will enable us to directly reference smaller issues back to the parent epic and see a timeline of how long an epic has been in progress.

Similar to the existing process, most Epics will be created based on feedback coming from customer or the companies strategic roadmap. We will not be making use of hard defined start and end dates but will instead rely upon the 'Inherited' dates based on work done by the teams. The Epics will also provide us with a tally for the amount of work that has been spent based on the weights of all the issues associated to the Epic which will be used for a number of reporting purposes.
