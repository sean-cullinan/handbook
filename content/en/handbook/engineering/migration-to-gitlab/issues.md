---
title: "Issues"
description: "How we are going to manage engineering issues within GitLab"
date: 2020-08-19T12:03:17Z

type: "docs"
no_list: true
---

[Issues](https://docs.gitlab.com/ee/user/project/issues/) are going to be the central location for all the work that is going to be done, or has been done, on the application. Just as Favro cards are used now, they will be created based on breakdown sessions and as required by engineers to record work that is being done. GitLab issues have the ability to add [labels]({{< ref "handbook/engineering/labels" >}}) for organisation, [weights]({{< ref "handbook/engineering/weights" >}}) to track work required and associations to [Epics]({{< ref "handbook/engineering/migration-to-gitlab/epics" >}}) and [Milestones]({{< ref "handbook/engineering/migration-to-gitlab/milestones" >}}).

Engineering team boards will be created to only show cards for each domain, and will be customised to represent the workflow that the teams are currently using for their Kanban process in Favro. The kanban boards in GitLab are based on labels and can be easily configured to support any columns as needed.


Epics natively support a relationship to issues that can be used to track the work spent to deliver on that project. Epics will automatically track the dates of cards that are related to it, allowing us to see how long a project has taken.

In GitLab there are [Milestones](https://docs.gitlab.com/ee/user/project/milestones/#milestones-as-agile-sprints) that can be used as Sprint time boxes. Sprints in Favro are tracked using a label and don't offer a way to see the dates that a particular Sprint was active.

Individual issues within GitLab can have many relationships to other cards and can be set as either being `related` or `blocking`/`blocked by` allowing a clear reference between an issue and why it is currently un-workable.
