---
title: "Milestones"
description: "How Milestones will be used to track sprints"
date: 2020-08-24T10:34:23Z

type: "docs"
no_list: true
---

In GitLab there are [Milestones](https://docs.gitlab.com/ee/user/project/milestones/#milestones-as-agile-sprints) that can be used as Sprint time boxes. Sprints in Favro are loosely tracked using labels and don't offer any functionality, such as seeing the dates that a particular Sprint was active, burn up/down based on issue weights worked.
