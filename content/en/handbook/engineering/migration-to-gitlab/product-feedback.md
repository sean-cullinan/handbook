---
title: "Product Feedback"
description: "How product feedback will be handled within GitLab"
date: 2020-08-19T12:03:30Z

type: "docs"
no_list: true
---

GitLab provides a feature called [Service Desk](https://docs.gitlab.com/ee/user/project/service_desk.html) that will be used to capture product feedback directly from within the application rather than the existing Airtable form that is being used. All feedback created this way will automatically be created as [Issues](https://docs.gitlab.com/ee/user/project/issues/) within GitLab and will be available for both Customer Success and Engineering teams. These issues will be processed in a similar way as the existing Airtable items where Engineering teams can process the list of uncategorised items and where appropriate create their own [Epics](https://docs.gitlab.com/ee/user/group/epics/index.html) based off the original feedback item.

Moving the product feedback and Engineering epics into GitLab will allow a much tighter link between the items and will allow clearer visibility over the progress of work towards a resolution. This will support Customer Success teams when engaging with customers and help to answer questions as they come in.

## Niceties

### Simpler product feedback form for customers

Moving product feedback into the application will allow us to remove a number of fields that are currently required in the Airtable form. The `Name` and `Email` fields can be removed and automatically filled based on the logged in user. We are also able to get some extra information such as the tenant name, without needing to ask for it. Feedback coming in can be automatically labelled to aid in the organisation of the items.
