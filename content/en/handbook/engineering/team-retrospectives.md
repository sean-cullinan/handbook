---
title: "Team Retrospectives"
date: 2020-08-04T23:57:25Z

type: "docs"
no_list: true
---

As part of the intelliHR development process we run a retrospective at the end of each sprint. Each team runs their own retrospective along with interested stakeholders, e.g. the CCO or CEO may be involved from time to time. The goal of the retrospective is to discuss what was [Liked](#liked), [Learned](#learned), [Loathed](#loathed) and [Longed For](#longed-for) over the previous sprint.

## Requirements for an efficient retrospective

When being involved in a retrospective it is important to keep something in mind:

- Everyone should have a chance to speak
- Everyone should feel comfortable to bring forward and discuss any points they want to raise
- Issues raised should have clear actions to resolve them

## Establishing a safe environment

Retrospectives are intended to bring up discussions about how the existing process and work is performing. Some retrospectives will have a majority of [Liked] cards and the discussion will move along without issue, however, that can't always be the case. Some sprints will have issues, large or small, that need addressing and these are just, or even more, important than the sprints without them. Issues that are left unresolved will hold the team back, reduce moral, and stop them from improving.

During the meeting it is the role of the team leader/engineering manager to make sure that discussions continue to move forward and that everyone involved has their chance to speak. They should try to remain impartial to help ensure that others are comfortable in voicing any concerns that they may have.

## Running a retrospective

Retrospectives are run in [Miro](https://miro.com) and the process is broken down into a few parts

 - [Creating a new Miro table](#creating-a-new-miro-table)
 - [Reviewing team boards](#reviewing-team-boards)
 - [Reflection time](#reflection-time)
 - [Round Table](#round-table)

### Creating a new Miro table

Each team has their own Miro board that contains a table for each sprint that has passed. In this table are 4 sections for team members to added notes into and 1 section for any retrospective actions that may be created during discussions.

![Miro Retro Board](/Miro-Retro-Board.png)

The 4 sections are to collect notes related to 1 of the 4 "feelings" - [Liked](#liked), [Learned](#learned), [Loathed](#loathed) and [Longed For](#longed-for) - that are used as inspiration when thinking back over the past sprint.

At the start of the retrospective the moderator should create a new copy of the table and set the title for the board to match the sprint that has just finished.

### Reviewing team boards

Once Miro is ready it is usual for the teams to go over the work that has been completed. This may either be done together as a team or individually during the [Reflection time](#reflection-time). Reviewing the boards lets everyone get a clear understanding of what has been completed, what is still in progress, if anything didn't get started and what has been added to the sprint that wasn't originally planned.

If this is performed as a team then it can also raise some discussion about the cards, although this should try to be limited to clarifications about the card itself rather than talking about how people feel about it.

### Reflection time

Once the boards have been reviewed there is time for the team members to contemplate their thoughts and feelings about the sprint and make notes as they see fit. These notes can be as short or long as required but should be kept to the side until the [round table](#round-table).

#### Liked

Use this space for anything that has gone well or made this sprint memorable e.g.

- The team was able to get through more work that expected
- Mention any well run meetings
- Someone went above and beyond to help out

#### Learned

Use this space to mention things that you or the team has leant

- You learnt about a new technology that can be used
- Started working in a new area of the platform and found something out
- Some personal development projects you've been looking at

#### Loathed

Use this space for anything that has gone wrong during the sprint and needs addressing

- Planning for a feature didn't go to plan
- Extra distractions stopping you from focusing on your tasks
- A piece of code you're working with has been causing issues
- A bug was released to production which was missed in testing

#### Longed For

Use this space for things that have been missing or have been hindering your work

- A piece of software you needed but couldn't get
- A process you weren't aware of
- Needed help but were unable to find someone to assist you
- Knowledge gap in the team that needs addressing

### Round Table

Once the time is over, everyone should have their notes ready. Someone will go first and read their card as the start of a possible discussion. The notes can be presented in any order, there is no need to start with [Liked](#liked). Once the team member has finished reading their note and mentioned anything they want to about it other team members can bring any related notes that they may have written. These extra notes may not belong in the same area of the board, some people may respond differently to similar situations. This is to be expected and can be used for further discussion.

As the notes are added to the board the moderator should be adding any action items that the team has brought up. As they are added the team should agree that what has been written correctly reflects what they believe to be the next steps to resolve the comments raised.

The round table continues with the next team member bringing another note to the board.

### Closing up

After all notes have been added to the board and discussions have finished the moderator should cover any actions that have been raised and make sure that all team members agree on the actions to be taken. The moderator should then create issues based off all actions that have been raised in the appropriate locations to ensure that they are followed up and documented. Any related parties should be notified via assignment or a comment on the issue about them to raise their visibility.

On the teams issue board the Engineering Manager will make sure that any work that has been completed and released is moved into the `closed` column. Any issues that are still in progress will have their [Milestone]({{< relref "/handbook/engineering/migration-to-gitlab/milestones.md" >}}) updated to the newly started milestone and the issue board will have its scope changed to show cards for the new milestone.
