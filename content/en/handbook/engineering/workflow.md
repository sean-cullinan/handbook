---
title: "Workflow"
date: 2020-08-29T11:01:03Z

type: "docs"
no_list: true
---

## Basics

1. Attend your teams daily stand-up. Here you will discuss work that was done yesterday, work remaining on the board to be done today, and any issues that might be blocking you
1. Start working on an issue that has been assigned to you. If you do not currently have any issues assigned to yourself, review the existing issues on your teams board and start working on the issue with the highest priority.
   - If you are still unsure what to work on talk with your team
1. Once you have a issue to start working on:
   - Make sure to move the issue along the board, or apply the `::In Progress` label
   - Assign the issue to yourself
   - Continue to work on the issue until it is ready for review
   - If you find that the list of changes in the one issue is large, you should try to split it into smaller issues
   - Once everything is ready for review, open up a new Merge Request
     - Fill in the title and description, describing why the change is being made, not what is being changed
     - Assign yourself and the reviewers for the MR, this will normally be from your team but may need reviewers from other teams
     - Add any labels as required
   - Update the issue by moving it along the board, or apply the `::In Review` label
   - During review it is possible that there will be comments left, go over the comments and address any feedback given
   - Once all the threads have been resolved and all approvers have approved the changes, the MR can be merged
   - Update the issue by moving it along the board, or apply the `::Merged` label
1. The team is responsible for making sure that any changes merged work correctly and do not introduce any new bugs.
   - Any new features or changes to the existing platform should have their own QA cards written or updated
   - Once a MR has been merged it is a good idea to test the change on the dev stack. It is possible that changes can work on an engineers computer but have issues in a production environment
1. If you have been working on an issue that came from Product Feedback mention the `@customer-success` team in the issue comments and update them on the progress. They may be waiting to hear back so they can communicate to the customer.

You are responsible for any issues assigned to you. If there is anything you are unsure about or require help with reach out to your team, Engineering Manager or Product Manager. It is always better to communicate early rather than waiting.

## Team Development Timeline

All the engineering teams work on a 2 to 4 week sprint. During this time the teams work on completing the epics that have been assigned to the team for the current sprint. This work may include development of a feature, fixing bugs, or starting investigation and break down of an upcoming feature.

Every Tuesday the Release Manager will start a new release that will deploy any updates into the production environment. While the release is being tested automatically each team will also be performing their own tests in the UAT stack. Each team will write a list of new features, bug fixes or enhancements that will be released and inform the Release Manager so that the release notes can be compiled. Once testing has been completed, the release will be promoted into production and the company will be informed of all the changes in the [#release-notification](https://intellihr.slack.com/archives/CU0DGU4HF) channel.

## Planning

At the start of every sprint the team will get together for a planning session. Here the engineers, engineering/iteration manager and product owners will discuss the work to be done during the next sprint. At the start of the planning meeting there will be a check of availability of the team members to help with estimating the amount of work that can be taken on. Together the group will then check for any new product feedback and if applicable to the team add it to their list of epics. The list of epics will then be prioritised to see if they need re-organising. Once an order has been agreed upon the team will estimate how much work they think can be delivered and assign the appropriate epics to the current milestone.

## Retrospective

At the end of a sprint teams will run their [Team Retrospective]({{< relref "/handbook/engineering/team-retrospectives" >}}) to reflect on the last sprint and to collect any feedback that may need actioning in future sprints.

## Milestone Clean-up

Engineering Managers are responsible for scheduling their teams planning and retrospective sessions, and ensuring that everything is ready for the start of the meetings.

- At the start of the day of the retro/planning session, all open issues that have not been completed should have their milestone updated to point to the upcoming sprint
- A new milestone should be created for the next sprint (there should always be a least one open sprint for each team)
- The milestone for the sprint that has just finished should be closed
