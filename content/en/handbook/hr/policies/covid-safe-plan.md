---
title: "COVID Safe Plan"
description: "How we are maintaining a safe working environment during COVID"
date: 2020-08-31T10:30:58Z

type: "docs"
no_list: true
---

The intelliHR office at Level 28, 345 Queen Street Brisbane Queensland is available to all staff on all working days with the safety measures as described in this plan. Please note that Working from the office (WFO) is optional and not an expectation. This is the current policy and this position will be reviewed periodically in the case there are any changes to government advice.

## WFO Declaration

Where you seek to WFO you will need to go to intelliHR platform the morning of WFO and complete the [WFO Declaration](https://internal-intellihr.intellihr.net/dashboard/self_service/perform/f23615db-073a-41c3-9a78-8cbf9251fc71). This must be completed each and every day you come into the office, and most importantly before entering the premises.

![WFO Declaration in intelliHR platform](/covid-safe-plan-wfo-declaration.png)

## WFO Conditions and Safety Measures

The following strict conditions and rules apply. They are designed to ensure you return to work in the office in a safe manner which continues to minimise the risk of COVID infection for all.

- If you or a family member or a person that you have had contact with in the past 14 days, has been tested for COVID and is positive for COVID or still awaiting results, you are not permitted to come into the office
- If you are feeling sick in any way – you are expressly not permitted to go to the office. If you come to the office showing any of these symptoms you will be immediately sent home.
- If you have any cold/flu like symptoms including fever, cough, sore throat, runny nose please stay home, you are expressly not permitted to go to the office. If you come to the office showing any of these symptoms you will be immediately sent home.
- If you have any one of the known COVID symptoms - Fever or Dry Cough or Shortness of Breath or Tiredness - you are expressly not permitted to go into the office, even if you believe you feel well. We also advise you seek medical attention. If you come to the office showing any of these symptoms you will be immediately sent home.
- Paul Trappett is the on-site full time leadership team representative and is authorised to send team members home immediately if displaying any signs of sickness.
- You must only work at your own designated desk.
- No Visitors or non-intelliHR staff are permitted in the office at all without the express permission of either the CEO or COO.
- If in the office, you have to maintain 1 person per 2 square meters.
- All meeting rooms with the exception of the board room, can only have 2 people in them at a time. The boardroom may have 5 people.
- Please regularly wash or sanitise your hands.
- You must use the hand sanitiser stations that are available at each entry to the building.
- You must use hand sanitiser immediately upon entry to the office. This and wipes will be available in the office.
- We encourage you to be flexible with your travel times if you are using public transport to avoid peak times.
- We encourage you to download and use the Governments COVID safe app to aid in early detection of any potential transmission
- No office food will be centrally stored in the kitchen for the time being to reduce the potential of contamination. Team members are able to use the fridges for their personal food.
- All glasses, cups and plates must be placed directly in the dishwasher once you are finished with them. The dishwasher is to be placed on the heavy cycle when full.
- Please note only 4 people can be in the kitchen at one time.

## Possible COVID Contact, Positive or Awaiting Test Results

If you have been in the office and become aware you may have been in contact with a person who may be linked to a case of transmission, or a person who is required to test or isolate or has tested positive for COVID you are required to immediately contact your supervisor, so we can ensure we keep everyone safe.

## What to do if you are unsure

If you have any questions or are unsure about anything, please contact your manager or any member of the leadership team for assistance. Your safety and that of your team mates are our priority.
